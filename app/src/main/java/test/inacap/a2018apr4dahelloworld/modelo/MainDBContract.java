package test.inacap.a2018apr4dahelloworld.modelo;

import android.provider.BaseColumns;

public class MainDBContract {
    private  MainDBContract(){}

    public static class MainDBUsuarios implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }
}

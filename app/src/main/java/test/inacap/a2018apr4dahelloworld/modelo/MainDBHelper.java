package test.inacap.a2018apr4dahelloworld.modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MainDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MainDBAPR4DA2018.db";
    public static final int DATABASE_VERSION = 1;

    public static final String SQL_CREATE_TABLE_USUARIOS =
            "CREATE TABLE " + MainDBContract.MainDBUsuarios.TABLE_NAME +
            " (" + MainDBContract.MainDBUsuarios._ID +" INTEGER PRIMARY KEY," +
                    MainDBContract.MainDBUsuarios.COLUMN_NAME_USERNAME + " TEXT," +
                    MainDBContract.MainDBUsuarios.COLUMN_NAME_PASSWORD + " TEXT)";

    public MainDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(this.SQL_CREATE_TABLE_USUARIOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO: Programar como deberia actualizarse la base de datos.
    }
}

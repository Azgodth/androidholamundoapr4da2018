package test.inacap.a2018apr4dahelloworld;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.a2018apr4dahelloworld.controlador.UsuarioControlador;

public class MainActivity extends AppCompatActivity {

    public EditText etNombreUsuario, etPassword;
    public TextView tvNombreUsuario, tvRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesiones.getBoolean("yaInicioSesion", false);
        if(yaInicioSesion){
            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }

        this.etNombreUsuario = findViewById(R.id.etNombreUsuario);
        this.etPassword = findViewById(R.id.etContrasena);
        this.tvNombreUsuario = findViewById(R.id.tvNombreUsuario);
        this.tvRegistrar = findViewById(R.id.tvRegistrar);

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent
                Intent intentNuevaVentana = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(intentNuevaVentana);
            }
        });

        Button btIngresar = findViewById(R.id.btIngresar);
        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPassword.getText().toString();

                UsuarioControlador controlador = new UsuarioControlador(getApplicationContext());
                int login = controlador.login(nombreUsuario, password);

               if(login == 0){
                   // Inicio sesion

                   // Preferencias compartidas
                   SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
                   SharedPreferences.Editor editor = sesiones.edit();
                   editor.putBoolean("yaInicioSesion", true);
                   editor.commit();


                   Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
                   startActivity(i);
                   finish();
               }
            }
        });
    }
}
